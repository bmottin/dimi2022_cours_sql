--
-- FONCTION
--

-- SUM
-- Sur une seule ligne 
-- il va faire la somme des likes où l'id de l'utilisateur est égale à 1
SELECT
    user_id,
    SUM(alikes)
FROM blog
WHERE
    user_id = 1;

-- Si je veut pour toutes les lignes, 
-- il me faut utiliser GROUP BY (un aggregateur)
-- pour regrouper les lignes par user_id
SELECT
    user_id,
    SUM(alikes)
FROM blog
GROUP BY user_id;

-- COUNT
-- Compte le nombre de post par user_id
SELECT
    user_id,
    count(user_id)
FROM 
    blog
GROUP BY user_id;

-- Moyenne
-- AVG
SELECT
    user_id,
    AVG(alikes) as 'moyenne des likes',
    COUNT(id) as 'Nombre de post',
    SUM(alikes) as 'total des likes'
FROM
    blog
GROUP BY 
    user_id;
-- avec jointure
SELECT
    user_id,
    nickname,
    AVG(alikes) as 'moyenne des likes',
    COUNT(blog.id) as 'Nombre de post',
    SUM(alikes) as 'total des likes'
FROM
    blog
INNER JOIN 
    user
ON
    user.id = blog.user_id
GROUP BY 
    user_id;