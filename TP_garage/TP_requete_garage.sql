-- -------------------------------------------
-- TP SQL
-- Completer les requetes en suivant la demande 
--  -------------------------------------------

-- Lister tous les clients
-- difficulté (1)
SELECT
    *
FROM 
    customers;

-- ********************************************************************

-- Lister les clients en limitant à 3
-- difficulté (1)
SELECT
    *
FROM 
    customers
limit 3;
    
-- ********************************************************************

-- Lister 3 derniers clients
-- difficulté (2)
SELECT
    *
FROM 
    customers
ORDER BY
    id DESC
limit 3;
    
-- ********************************************************************

-- lister tout les employées mais seulement nom et prénom (et les noms de colonne correspondant)
-- difficulté (1)
SELECT 
    lastname as "Nom de famille",
    firstname as "Prénom"
FROM
    employees;
-- ********************************************************************

-- lister tout les employees avec leurs fiches de postes. (jointure)
-- difficulté (3)
SELECT firstname, lastname, type_posts.name
FROM employees
INNER JOIN
type_posts
ON
type_posts.id = employees.type_post_id;

-- ********************************************************************

-- afficher l'employee avec l'id 4 
-- difficulté (3)
SELECT * FROM employees WHERE id = 4;
-- ********************************************************************

-- trouver les employés avec le poste mécano
-- 2 maniere de faire (avec l'id ou avec l'intitulé du poste) 
-- (faire les 2)
-- difficulté (5)

-- id
SELECT * FROM employees
INNER JOIN 
type_posts
ON
type_post_id = type_posts.id
WHERE type_posts.id = 2;

-- intitulé
SELECT * FROM employees
INNER JOIN 
type_posts
ON
type_post_id = type_posts.id
WHERE 
 name = 'm_cano';
-- pourquoi la 2eme maniere est mieux ?
-- On fait abstraction de l'ID qui est une valeur volatile

-- ********************************************************************

-- trouver les devis non transformés en facture (where)
-- difficulté (5)
SELECT id FROM invoices
where `date_accepted` IS NULL;

-- ********************************************************************

-- trouver les devis creer en juin (where)
-- difficulté (5)
-- soit avec like 
SELECT * from invoices 
where date_created like '2020-06-%';
-- soit avec between

SELECT * FROM invoices
WHERE date_created between '2020-06-01' AND '2020-06-30';


-- ********************************************************************

-- afficher la facture avec l'id 3 avec toutes les infos nécéssaire 
-- à la generation de la facture papier 
-- (info garage, du client, les interventions, les lignes de la facture, etc.)
-- difficulté (10)
SELECT 
    * 
from invoices
INNER JOIN 
    invoice_lines il
ON
    invoices.id = il.invoice_id
INNER JOIN
    cars
on 
    cars.id = invoices.car_id
INNER JOIN
    customers
ON 
    customers.id = cars.custumer_id

INNER JOIN
    interventions
ON 
    cars.id = interventions.car_id
INNER JOIN 
    employee_intervention_rel eir
ON
    interventions.id = eir.intervention_id
INNER JOIN
    employees
ON 
    employees.id = eir.employee_id
INNER JOIN 
    garages
ON
    garages.id = employees.garage_id
WHERE 
    invoice_id = 3

-- ********************************************************************

-- Ajouter une clé etrangere entre client et sa voiture
ALTER TABLE
    cars
ADD FOREIGN KEY (custumer_id) references customers(id)

-- 
ALTER TABLE 
    employees
ADD CONSTRAINT FK_employee_garage FOREIGN KEY (garage_id) references garages(id);
-- ********************************************************************


-- calculer le montant de la facture numéro 5
-- difficulté (over 9000)
SELECT
    SUM(`unit_price` * quantity)
    FROM invoice_lines where invoice_id = 5;



