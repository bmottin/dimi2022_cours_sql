-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mar. 01 sep. 2020 à 15:06
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `test_import`
--

--
-- Déchargement des données de la table `cars`
--

INSERT INTO `cars` (`id`, `marque`, `modele`, `immatriculation_num`, `grey_card_num`, `custumer_id`) VALUES
(1, 'Renault', 'Super 5', 'VV 404 NF', '123456789045686', 1),
(2, 'Tesla', 'Modele 1', 'DV 666 HL', '23456789105676846', 2),
(3, 'Dodge', 'Viper', 'NA 505 NA', '98745689765461265', 3),
(4, 'Porsche', 'Boxter', 'OK 202 KO', '45968956126165798', 4),
(5, 'Mini', 'Cooper', 'MAR 10 BRO', '56789765654986', 2),
(6, 'Citröen', 'C3', 'BOO 101 NO', '4897412546716769', 3);

--
-- Déchargement des données de la table `customers`
--

INSERT INTO `customers` (`id`, `firstname`, `lastname`, `telephone`, `email`, `street1`, `street2`, `postal_code`, `city`) VALUES
(1, 'Luc', 'Coursleciel', '0233060607', 'lcourleciel@deathstar.fr', '1 rue corruscent', NULL, '50000', 'Saint Lô'),
(2, 'Alf', 'Cateater', '02.33.07.68.08', 'a.catapasta@gmail.com', '1 rue du pou qui grimpe', NULL, '50200', 'Coutances'),
(3, 'Clark', 'Kent', '02.33.08.45.12', 'k.kent@dailyplanet.com', '42 rue du cristal vert', 'lieu-dit \"le coin paumé\"', '50180', 'Hébécrevons'),
(4, 'Peter', 'Parker', '02.33.15.18.39', 'a.regner@nyu.com', '50 rue du vieux chalet', NULL, '61', 'Alençon');

--
-- Déchargement des données de la table `employees`
--

INSERT INTO `employees` (`id`, `firstname`, `lastname`, `type_post_id`, `garage_id`) VALUES
(1, 'Wylis', 'HODOR', 2, 1),
(2, 'Denis', 'BOUCHER', 2, 1),
(3, 'Oswald', 'COBBLEPOT', 3, 1),
(4, 'Charles', 'LATANS', 1, 1);

--
-- Déchargement des données de la table `employee_intervention_rel`
--

INSERT INTO `employee_intervention_rel` (`employee_id`, `intervention_id`, `duration`, `action_name`) VALUES
(1, 2, NULL, 'vidange '),
(1, 3, 600, NULL),
(1, 4, 2, 'entretien'),
(1, 5, 60, 'Vidange'),
(1, 6, 2, 'CHangement roue + entretien'),
(1, 10, NULL, 'Vidange'),
(2, 3, 75, 'Vidange'),
(2, 8, 1, 'Changement bloc feu'),
(2, 9, 75, 'entretien'),
(3, 1, 750, 'Peinture'),
(3, 10, 75, 'Peinture pc');

--
-- Déchargement des données de la table `garages`
--

INSERT INTO `garages` (`id`, `name`, `siret`, `telephone`, `email`, `street1`, `street2`, `postal_code`, `city`) VALUES
(1, 'Garage Charles Latans', '0123456789', '02.33.82.91.12', 'c.latans@wanadoo.fr', '10 rue Lycette D\'arsonval', NULL, '50000', 'Saint Lô');

--
-- Déchargement des données de la table `interventions`
--

INSERT INTO `interventions` (`id`, `description`, `car_id`) VALUES
(1, 'Débossage aile avG\r\npeinture complete', 1),
(2, 'Vidange', 2),
(3, 'vidange', 4),
(4, 'Entretien 30 000 KM', 3),
(5, 'Changement moteur\r\nCourroie Distrib', 4),
(6, 'Entretien 200 000 KM\r\nChangement de roue', 5),
(7, 'Changement porte AVG\r\nPeinture', 6),
(8, 'Bloc feu\r\nparechoc', 5),
(9, 'préparation CT\r\n', 3),
(10, 'Peinture Parechoc', 4);

--
-- Déchargement des données de la table `invoices`
--

INSERT INTO `invoices` (`id`, `car_id`, `date_created`, `date_updated`, `date_accepted`, `quote_number`, `invoice_number`) VALUES
(1, 1, '2020-06-01 15:17:31', '2020-06-02 15:17:31', '2020-06-02 15:17:31', 10, 1),
(2, 2, '2020-06-09 15:17:31', '2020-06-09 15:17:31', '2020-06-09 15:17:31', 11, 2),
(3, 4, '2020-06-19 15:19:46', '2020-06-19 15:19:46', '2020-06-19 16:19:46', 12, 3),
(4, 3, '2020-06-21 15:19:46', '2020-06-21 15:19:46', '2020-06-21 15:19:46', 13, 4),
(5, 5, '2020-07-08 16:26:18', '2020-07-16 16:26:18', '2020-07-16 16:26:18', 15, 5),
(6, 6, '2020-08-02 16:26:18', '2020-08-02 16:26:18', '2020-08-02 16:26:18', 16, 6),
(9, 4, '2020-08-04 17:33:08', '2020-08-04 17:33:08', NULL, 16, 7),
(10, 4, '2020-05-25 17:33:08', '2020-08-25 17:33:08', NULL, 1, NULL);

--
-- Déchargement des données de la table `invoice_lines`
--

INSERT INTO `invoice_lines` (`id`, `designation`, `unit_price`, `quantity`, `sort_order`, `invoice_id`) VALUES
(1, 'Carrosserie T1', '40.00', '2.50', 1, 1),
(2, 'Carrosserie T3', '65.00', '10.00', 2, 1),
(3, 'Forfait Vidange tout compris', '100.00', '1.00', 1, 2),
(4, 'Forfait Vidange tout compris', '100.00', '1.00', 1, 3),
(5, 'Forfait Entretien', '75.00', '1.00', 1, 4),
(6, 'Mecanique', '65.00', '10.00', 1, 3),
(7, 'Forfait entretien', '100.00', '1.00', 1, 5),
(8, 'Changement roue', '80.00', '2.00', 2, 5),
(9, 'Préparation CT', '100.00', '1.00', 1, 6),
(10, 'Carrosserie T3', '75.00', '3.00', 1, 10);

--
-- Déchargement des données de la table `type_posts`
--

INSERT INTO `type_posts` (`id`, `name`) VALUES
(1, 'gérant'),
(2, 'mécano'),
(3, 'peintre');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
