-- Commentaire

-- 
-- SELECT 
-- Le select permet de faire de la consultation de la BDD
-- Il n'y aucune modification de faite 
--

-- * -> correspond à un 'joker' qui prend tout les champs
SELECT * FROM blog;

-- on peut préciser les champs à recuperer
SELECT id, content FROM blog;

-- renomme les colones, juste pour l'affichage de la requête en cours
SELECT 
    id as 'identifiant du blog', 
    content as 'Contenu' 
FROM 
    blog;

-- je limite le select pour les 3 premieres lignes
SELECT id, content FROM blog limit 3;

-- on peut changer l'ordre des enregistrements 
SELECT 
    id, content 
FROM 
    blog 
ORDER BY
    id DESC; -- ASC

--
-- Clause WHERE 
-- permet de limiter la recherche 
-- aux enregistrements qui correspondent
--
SELECT 
    id as 'ref',
    content as 'citation'
FROM
    blog
WHERE
    id = 10;

-- Like
-- permet trouver des correspondances
-- % permet d'indiquer d'ignorer ce qui se trouve avant et/ou après
-- suivant la position
SELECT 
    *
FROM 
    user
WHERE 
    nickname like 'Ben%';

-- _ permet d'échaper un seul caractere
SELECT 
    *
FROM 
    user
WHERE 
    nickname like 'beno_t';

-- in
-- in permet de recherche des valeur dans la collection fournie
SELECT * FROM blog
where
    id in (2,5,7);

-- IS NULL
SELECT * FROM user
where
    `profile_pic` IS NULL;

-- IS NOT NULL
SELECT * FROM user
where
    `profile_pic` IS NOT NULL;

-- Condition
SELECT * FROM blog
where
    alikes >= 15;

SELECT * FROM blog
where
    alikes <> 20; -- different de 20

-- BETWEEN
SELECT * FROM blog
where
    alikes 
BETWEEN 
    5 AND 10; -- compris entre 5 et 10

--
-- INSERT 
--

INSERT INTO blog(content, user_id, alikes)
values ('Coucou', 1, 99); -- pas de date created ni ID car gerer par le moteur BDD

INSERT INTO blog(id, content, user_id, alikes, date_created)
values (250, 'Yatta', 1, 99, '2021-09-01'); -- Mais on peut le préciser si besoin

-- UPDATE
UPDATE  
    user
SET
    nickname = 'Super Benoît'
WHERE 
    id = 3;

-- DELETE

DELETE FROM
    `user`
WHERE 
    id = 4;

-- Toutes les requêtes dans ce fichier font parties du CRUD 
-- (Create Read Update Delete)
