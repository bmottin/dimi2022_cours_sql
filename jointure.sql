-- Les jointures permettent de relier 2 tables 
-- entre elle sur une paire de clé

SELECT
    blog.id, content, nickname, user_id
 FROM 
    blog
INNER JOIN
    user
ON
    user.id = blog.user_id;

